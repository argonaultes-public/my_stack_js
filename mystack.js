class MyStack {
    #nb_items = 0;
    #item = null;

    isEmpty() {
        return this.#nb_items === 0;
    }

    pop() {
        const item = this.#item;
        if (item == null) {
            return null;
        }
        this.#item = item.previous;
        this.#nb_items--;
        return item.value;
    }

    push(value) {
        const newValueItem = new MyStackItem(value);
        newValueItem.previous = this.#item;
        this.#item = newValueItem;
        this.#nb_items++;
    }
}

class MyStackItem {
    #value;
    #previous;

    constructor(value) {
        this.#value = value;
    }

    get previous() {
        return this.#previous;
    }

    set previous(previous_item) {
        this.#previous = previous_item;
    }

    get value() {
        return this.#value;
    }

}

module.exports = MyStack;