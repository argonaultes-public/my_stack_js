Pour construire l'image Docker

```bash
docker build -t mystack .
```

Pour démarrer l'image Docker en interactif

```bash
docker run --rm -i -t mystack bash
```