const MyStack = require('./mystack');

test('isEmpty', () => {
    const my_stack = new MyStack();
    expect(my_stack.isEmpty()).toBe(true);
});

test('single Add', () => {
    const item = "new item 1";
    const my_stack = new MyStack();
    my_stack.push(item);
    expect(my_stack.isEmpty()).toBe(false);
    const item_pop = my_stack.pop();
    expect(item_pop).toBe(item);
    expect(my_stack.isEmpty()).toBe(true);
    expect(my_stack.pop()).toBe(null);
});

test('single several items', () => {
    const my_stack = new MyStack();
    const item = 'item_';
    for (let i = 0 ; i < 4 ; i++) {
        my_stack.push(item + i);
    }
    expect(my_stack.isEmpty()).toBe(false);
    for (let i = 3 ; i >= 0 ; i--) {
        let item_pop = my_stack.pop();
        expect(item_pop).toBe(item + i);
    }
});

test('pop on empty', () => {
    const my_stack = new MyStack();
    expect(my_stack.pop()).toBe(null);
});

